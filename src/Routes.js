import React from "react";
import { View, Platform, Image } from "react-native";
import {
  createAppContainer,
  createSwitchNavigator,
  NavigationActions
} from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createBottomTabNavigator } from "react-navigation-tabs";

import Login from "../src/Component/pages/login/Login";
//import Facebook from '../src/Component/pages/login/Facebook';
import Signup from "../src/Component/pages/login/Signup";
import ForgotPassword from "./Component/pages/ForgotPassword/ForgotPassword";
import ResetPassword from "./Component/pages/ForgotPassword/ResetPassword";
import changePassword from "./Component/pages/ForgotPassword/changePassword";
import Thankyou from "./Component/pages/login/Thankyou";

import index from "../src/Component/pages/Home/index";
//import MapScreen from '../src/Component/pages/Home/MapScreen';
import booking from "../src/Component/pages/Home/booking";
import Typeof from "../src/Component/pages/Home/Typeof";
import Masseuse from "../src/Component/pages/Home/Masseuse";
import details from "../src/Component/pages/Home/details";
import CardFormScreen from "../src/Component/pages/Home/CardFormScreen";

import AppointmentHistory from "./Component/pages/Customer/AppointmentHistory";
import UpcommingAppointment from "./Component/pages/Customer/UpcommingAppointment";
import editProfile from "./Component/pages/Customer/editProfile";
import myAccount from "./Component/pages/Customer/myAccount";
import Transection from "./Component/pages/Customer/Transection";
import cancelAppointment from "./Component/pages/Customer/cancelAppointment";

const LoginStack = createStackNavigator({
  Login: Login,
  Signup: Signup,
  ForgotPassword: ForgotPassword,
  ResetPassword: ResetPassword
  //Facebook: Facebook,
});

const paymentStack = createStackNavigator({
  CardFormScreen: CardFormScreen
});

const HomeStack = createStackNavigator(
  {
    index: index,
    details: details,
    booking: booking,
    Typeof: Typeof,
    Masseuse: Masseuse,
    //CustomCardScreen: CustomCardScreen,
  },
  {
    defaultNavigationOptions: {
      header: null
    }
  }
);

HomeStack.navigationOptions = {
  tabBarLabel: " ",
  tabBarOptions: {
    activeTintColor: "#e25cff",
    labelStyle: {
      fontSize: 0
    },
    style: {
      backgroundColor: "#23232c",
      borderWidth: 0
    }
  },
  tabBarIcon: ({ focused }) => (
    <Image
      style={{ height: 26, width: 26, marginTop: 10.5 }}
      source={
        focused
          ? require("./Component/assets/images/homeActive.png")
          : require("./Component/assets/images/home.png")
      }
    />
  )
};

const AppointmentHistoryStack = createStackNavigator({
  AppointmentHistory: AppointmentHistory
});
AppointmentHistoryStack.navigationOptions = {
  tabBarLabel: " ",
  tabBarOptions: {
    activeTintColor: "#e25cff",
    labelStyle: {
      fontSize: 0
    },
    style: {
      backgroundColor: "#23232c",
      borderWidth: 0
    }
  },
  tabBarIcon: ({ focused }) => (
    <Image
      style={{ height: 26, width: 26, marginTop: 10.5 }}
      source={
        focused
          ? require("./Component/assets/images/picon4Active.png")
          : require("./Component/assets/images/picon4.png")
      }
    />
  )
};
const UpcommingAppointmentStack = createStackNavigator({
  UpcommingAppointment: UpcommingAppointment
});
UpcommingAppointmentStack.navigationOptions = {
  tabBarLabel: " ",
  tabBarOptions: {
    activeTintColor: "#e25cff",
    labelStyle: {
      fontSize: 0
    },
    style: {
      backgroundColor: "#23232c",
      borderWidth: 0
    }
  },
  tabBarIcon: ({ focused }) => (
    <Image
      style={{  height: 26, width: 26, marginTop: 10.5 }}
      source={
        focused
          ? require("./Component/assets/images/picon3Active.png")
          : require("./Component/assets/images/picon3.png")
      }
    />
  )
};
const AccountStack = createStackNavigator(
  {
    myAccount: myAccount,
    changePassword: changePassword,
    editProfile: editProfile,
    Transection: Transection,
    AppointmentHistory: AppointmentHistory,
    UpcommingAppointment: UpcommingAppointment,
    cancelAppointment: cancelAppointment,
    Thankyou: Thankyou
  },
  {
    initialRoute: "myAccount"
  }
);
// const resetAction = NavigationActions.reset({
//   index: 0,
//   actions: [
//     NavigationActions.navigate({ routeName: 'myAccount'})
//   ] });

AccountStack.navigationOptions = {
  tabBarLabel: " ",
  tabBarOptions: {
    activeTintColor: "#e25cff",
    labelStyle: {
      fontSize: 0
    },
    style: {
      backgroundColor: "#23232c",
      borderWidth: 0
    }
  },
  tabBarIcon: ({ focused }) => (
    <Image
      style={{ height: 26, width: 26, marginTop: 10.5}}
      source={
        focused
          ? require("./Component/assets/images/pro-pink.png")
          : require("./Component/assets/images/pro-grey.png")
      }
    />
  )
};

const tabnavigator = createBottomTabNavigator({
  HomeStack,
  UpcommingAppointmentStack,
  AppointmentHistoryStack,
  AccountStack
});

const switchStack = createSwitchNavigator({
  tabnavigator,
  LoginStack,
  paymentStack,
  tabnavigator
});
const AppContainer = createAppContainer(switchStack);

export default class Routes extends React.Component {
  render() {
    return <AppContainer />;
  }
}
