import {combineReducers} from 'redux';
import spaReducer from '../../Component/pages/Home/redux/spaReducer';
import authReducer from '../../Component/pages/login/redux/authReducer';
import forgotPasswordReducer from '../../Component/pages/ForgotPassword/redux/forgotPasswordReducer';
import customerReducer from '../../Component/pages/Customer/redux/customerReducer';

// import postReducer from './postReducer';
// import authReducer from './AuthReducer';

export default combineReducers({
  spa: spaReducer,
  auth: authReducer,
  forgotPassword: forgotPasswordReducer,
  customer: customerReducer,
});
