import React, {Component} from 'react';
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  AsyncStorage,
  ScrollView,
  ImageBackground,
  Image,
} from 'react-native';
import {Avatar, Icon, ListItem} from 'react-native-elements';
import styles from './style';
import {connect} from 'react-redux';
import {fetchCustomerDetails} from './redux/customerAction';
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter,
} from 'react-native-popup-dialog';
import Axios from 'axios';

class myAccount extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      customer_id: '',
      visible1: false,
      popupMsg: '',
      pin: '',
      city: '',
    };
    this.locationName();
  }

  //----fetching customer details using redux----//
  async componentDidMount() {
    console.log('componentDidMount in myaccount page');

    this.props.navigation.addListener('willFocus', () => {
      console.log('focus', this.state.customer_id);
      if (this.state.customer_id) {
        console.log('focused working');
        let userDetails = new FormData();
        userDetails.append('user_id', this.state.customer_id);
        this.props.fetchCustomerDetails(userDetails);
      }
    });

    try {
      const value = await AsyncStorage.getItem('login_id_2');
      console.log('loginId in myaccount page', value);
      if (value) {
        this.setState({customer_id: value});
      } else {
        this.props.navigation.navigate('Login');
      }
    } catch (error) {
      console.log(error);
    }
    let userDetails = new FormData();
    userDetails.append('user_id', this.state.customer_id);
    this.props.fetchCustomerDetails(userDetails);
  }

  //  componentDidUpdate(prevProps) {
  //    console.log("componentDidUpdate",this.state.visible1);
     
  //   if (this.state.visible1 === false && this.state.pin === 1) {
  //     this.props.navigation.navigate('Login');
  //   }
  // }

  logOut = () => {
    console.log('logOut');
    AsyncStorage.removeItem('login_id_2');

    this.props.navigation.navigate('Login');

    // if (this.state.visible1 === false) {
    //   this.setState({
    //     visible1: true,
    //     popupMsg: 'Are You Sure You Want To Logout',
    //   });
    // }
  };

  // remove = () => {
  //   console.log('RemoveAsyncstorage', this.state.masseuse_id);
  //   this.setState({
  //     visible1: false,
  //     pin: 1,
  //   });
  //   AsyncStorage.removeItem('login_id');

  //   this.setState({customer_id: ''});
  // };

  locationName = () => {
    //current location fetch

    Axios.get('http://ip-api.com/json')
      .then(location => {
        console.log('location', location);
        this.setState({city: location.data.city});
      })
      .catch(err => {});
  };

  // closePopupbox = () => {
  //   this.setState({
  //     visible1: false,
  //   });
  // };

  render() {
    console.log('props in render in myaccount page', this.props);
    //const {goBack} = this.props.navigation;
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            {/* <TouchableOpacity
              onPress={() => this.props.navigation.navigate('index')}>
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity> */}
            <Text style={styles.backheading}>My Account</Text>
          </View>
        </View>
        <ScrollView>
          <View>
            <ImageBackground
              source={require('../../assets/images/pbg2.png')}
              style={styles.tprofile}>
              <View style={[styles.media, {marginBottom: 0}]}>
                {this.props.customerDetails.profile_image ? (
                  <Avatar
                    rounded
                    source={{
                      uri: `${this.props.customerDetails.profile_image}`,
                    }}
                    size={90}
                    containerStyle={{
                      borderWidth: 2,
                      borderColor: '#ed9bfe',
                      padding: 6,
                      backgroundColor: '#fff',
                    }}
                  />
                ) : (
                  <Avatar
                    rounded
                    source={require('../../assets/images/download.png')}
                    size={90}
                    containerStyle={{
                      borderWidth: 2,
                      borderColor: '#ed9bfe',
                      padding: 6,
                      backgroundColor: '#fff',
                    }}
                  />
                )}
                <View style={styles.mediabody}>
                  <View style={{position: 'relative', top: 25}}>
                    <Text style={[styles.h3, {color: '#fff'}]}>
                      {this.props.customerDetails.name}
                    </Text>
                    <TouchableOpacity style={{flexDirection: 'row'}}>
                      <Image
                        source={require('../../assets/images/locate.png')}
                        resizeMode="contain"
                        style={{width: 10, marginRight: 5}}
                      />
                      <Text style={{color: '#fadcff'}}>{this.state.city}</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </ImageBackground>
            <View style={styles.graycontainer}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('editProfile')}
                style={[{marginBottom: 0}]}>
                <View style={[styles.listitem]}>
                  <Image
                    source={require('../../assets/images/picon1.png')}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={[styles.menutext, styles.textLeft]}>
                    Profile
                  </Text>

                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('changePassword')}
                style={[{marginBottom: 0}]}>
                <View style={[styles.listitem]}>
                  <Image
                    source={require('../../assets/images/picon2.png')}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={[styles.menutext, styles.textLeft]}>
                    Security Settings
                  </Text>

                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </View>
              </TouchableOpacity>
              {/* <View style={styles.container}>
                <Dialog
                  visible={this.state.visible1}
                  dialogAnimation={
                    new SlideAnimation({
                      slideFrom: 'bottom',
                    })
                  }
                  onTouchOutside={() => {
                    this.closePopupbox();
                  }}
                  dialogStyle={{width: '80%'}}
                  footer={
                    <DialogFooter>
                      <DialogButton
                        textStyle={{
                          fontSize: 14,
                          color: '#333',
                          fontWeight: '700',
                        }}
                        text="Yes"
                        onPress={() => {
                          this.remove();
                        }}
                      />
                      <DialogButton
                        textStyle={{
                          fontSize: 14,
                          color: '#333',
                          fontWeight: '700',
                        }}
                        text="No"
                        onPress={() => {
                          this.closePopupbox();
                        }}
                      />
                    </DialogFooter>
                  }>
                  <DialogContent>
                    <Text style={styles.popupText}>{this.state.popupMsg}</Text>
                  </DialogContent>
                </Dialog>
              </View> */}

              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('UpcommingAppointment')
                }
                style={[{marginBottom: 0}]}>
                <View style={[styles.listitem]}>
                  <Image
                    source={require('../../assets/images/picon3Active.png')}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={[styles.menutext, styles.textLeft]}>
                    Upcoming Appointments
                  </Text>

                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('AppointmentHistory')
                }
                style={[{marginBottom: 0}]}>
                <View style={[styles.listitem]}>
                  <Image
                    source={require('../../assets/images/picon4Active.png')}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={[styles.menutext, styles.textLeft]}>
                    Appointment History
                  </Text>

                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Transection')}
                style={[{marginBottom: 0}]}>
                <View style={[styles.listitem]}>
                  <Image
                    source={require('../../assets/images/dollar.png')}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={[styles.menutext, styles.textLeft]}>
                    Transaction History
                  </Text>

                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('cancelAppointment')
                }
                style={[{marginBottom: 0}]}>
                <View style={[styles.listitem]}>
                  <Image
                    source={require('../../assets/images/cross.png')}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={[styles.menutext, styles.textLeft]}>
                    Cancelled Appointments
                  </Text>

                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={this.logOut}
                style={[{marginBottom: 0}]}>
                <View style={[styles.listitem]}>
                  <Image
                    source={require('../../assets/images/aicon7.png')}
                    resizeMode="contain"
                    style={styles.picon}
                  />
                  <Text style={[styles.menutext, styles.textLeft]}>
                    Log Out
                  </Text>

                  <Icon
                    name="ios-arrow-forward"
                    type="ionicon"
                    color="#4a4a54"
                  />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const mapStateToProps = state => ({
  ...state.customer,
});
export default connect(mapStateToProps, {fetchCustomerDetails})(myAccount);
