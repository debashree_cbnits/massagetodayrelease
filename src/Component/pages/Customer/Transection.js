import React, {Component} from 'react';
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Image,
  AsyncStorage,
  Alert,
} from 'react-native';
import {Avatar, Icon, CheckBox} from 'react-native-elements';
import styles from './style';
import {connect} from 'react-redux';
import {fetchTransactionHistory} from './redux/customerAction';
import Spinner from 'react-native-loading-spinner-overlay';
import moment from 'moment';


class Transection extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      spinner: true,
      customer_id: '',
    };
    todayDate = moment(new Date()).format('YYYY-MM-DD');
  }

  componentWillReceiveProps(nextProps) {
    console.log('componentWillReceiveProps', nextProps);
    this.setState({spinner: false});
  }

  //--FETCH TRANSACTION HOSTORY THROUGH REDUX--//
  async componentDidMount() {
    try {
      const value = await AsyncStorage.getItem('login_id_2');
      console.log('loginId in myaccount page', value);
      if (value) {
        this.setState({customer_id: value});
      }
    } catch (error) {
      console.log(error);
      // Error retrieving data
    }
    let userDetails = new FormData();
    userDetails.append('customer_id', this.state.customer_id);
    userDetails.append('todaysDate', todayDate);

    this.props.fetchTransactionHistory(userDetails);
  }

  render() {
    console.log('props in render in transaction page', this.props);
    console.log(
      'props in render in transaction page',
      this.props.transactionHistory.transaction_history,
    );
    const transaction = this.props.transactionHistory;
    console.log('constant transaction', transaction);

    const {goBack} = this.props.navigation;
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity onPress={() => goBack()}>
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>Transaction History</Text>
          </View>
        </View>
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <ScrollView>
          {/* {this.props.error && this.props.error !== undefined ? (
            Alert.alert('Something went wrong')
          ) : ( */}
            <View style={styles.graybg}>
              {this.props.transactionHistory.transaction_history && this.props.transactionHistory.transaction_history.length
                ? this.props.transactionHistory.transaction_history.map((item, key) => (
                    <View key={key} style={styles.lightgry}>
                      
                      <View style={styles.roundcontainer}>
                        <View style={styles.headergray}>
                          
                          <Text
                            style={[
                              styles.whitetext,
                              {fontSize: 22, fontWeight: '300'},
                            ]}>
                            {item.spa_name}
                          </Text>
                        </View>
                        <View style={styles.grbody}>
                          <View
                            style={[
                              styles.justifyrow,
                              {alignItems: 'flex-start', marginRight: 8},
                            ]}>
                            <Text style={styles.mutetexto}>
                              Transaction Id:
                            </Text>
                            <Text style={styles.whitetexto}>
                              {item.charge_id}
                            </Text>
                          </View>
                          <View
                            style={[
                              styles.justifyrow,
                              {alignItems: 'flex-start'},
                            ]}>
                            <Text style={styles.mutetexto}>
                            Booking Date :
                            </Text>
                            <Text style={styles.whitetexto}>
                              {item.booking_date}
                            </Text>
                          </View>
                          <View
                            style={[
                              styles.justifyrow,
                              {alignItems: 'flex-start'},
                            ]}>
                            <Text style={styles.mutetexto}>
                              {' '}
                              Booking Id :{' '}
                            </Text>
                            <Text style={styles.whitetexto}>
                              {item.booking_id}
                            </Text>
                            
                          </View>

                          <View
                            style={[
                              styles.justifyrow,
                              {alignItems: 'flex-start'},
                            ]}>
                            <Text style={styles.mutetexto}>
                              {' '}
                              Amount :{' '}
                            </Text>
                            <Text style={styles.whitetexto}>
                              {item.total_amount}
                            </Text>
                            
                          </View>
                        </View>
                      </View>
                    </View>
                  ))
                : 
                <Text style={styles.backheading}>
                  No Appointment History Found
                </Text>}
            </View>
          {/* )} */}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.customer,
});
export default connect(
  mapStateToProps,
  {fetchTransactionHistory},
)(Transection);
