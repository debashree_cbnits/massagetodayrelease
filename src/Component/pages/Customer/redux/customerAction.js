import {
  FETCH_CUSTOMER_DETAILS,
  EDIT_PROFILE,
  FETCH_TRANSACTION_HISTORY,
  FETCH_APPOINTMENT_HISTORY,
  FETCH_UPCOMMING_APPOINTMENT,
  CENCEL_BOOKING,
  CANCEL_BOOKING_LIST,
  ADD_REVIEW
} from '../../../../redux/actions/types';
import Api from '../../../../Api/Api';

export const addReview = reviewdata => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/addReview',
      reviewdata,
    );

    if (response) {
      console.log("add review",response);
      
      dispatch({type: ADD_REVIEW, payload: response});
    }
  } catch (error) {
    console.log(error, "addReview-error");
    dispatch({type: ADD_REVIEW, payload: {response: error}});
  }
};

export const fetchCustomerDetails = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/userProfileView',
      userDetails,
    );

    if (response) {
      console.log("userprofile",response);
      
      dispatch({type: FETCH_CUSTOMER_DETAILS, payload: response.profile_data});
    }
  } catch (error) {
    console.log(error);
    dispatch({type: FETCH_CUSTOMER_DETAILS, payload: {response: error}});
  }
};

export const customerUpdate = userDetails => async dispatch => {
  try {
    const response = await Api.postApi('webservice/users/userUpdateProfile', userDetails);
    if (response) {
      console.log(response);
      
      dispatch({type: EDIT_PROFILE, payload: {...response}});
    }
  } catch (error) {
    dispatch({type: EDIT_PROFILE, payload: {response: error}});
  }
};

export const fetchTransactionHistory = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/payments/customerTransactionhistory',
      userDetails,
    );

    if (response) {
      console.log("transaction_history",response);
      
      dispatch({type: FETCH_TRANSACTION_HISTORY, payload: response});
    }
  } catch (error) {
    console.log(error);
    dispatch({type: FETCH_TRANSACTION_HISTORY, payload: {response: error}});
  }
};

export const fetchAppointmentHistory = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/bookings/bookingHistory',
      userDetails,
    );

    if (response) {
      console.log("transaction_history",response);
      
      dispatch({type: FETCH_APPOINTMENT_HISTORY, payload: response.booking_history});
    }
  } catch (error) {
    console.log(error);
    dispatch({type: FETCH_APPOINTMENT_HISTORY, payload: {response: error}});
  }
};

export const fetchUpcommingAppointment = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/bookings/upcomingBookings',
      userDetails,
    );

    if (response) {
      console.log("fetchUpcommingAppointment",response);
      
      dispatch({type: FETCH_UPCOMMING_APPOINTMENT, payload: response.upcoming_bookings});
    }
  } catch (error) {
    console.log(error);
    dispatch({type: FETCH_UPCOMMING_APPOINTMENT, payload: {response: error}});
  }
};

export const cancelBooking = cancelId => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/payments/stripeChargeRefund',
      cancelId,
    );

    if (response) {
      console.log("cancelBookings",response);
      
      dispatch({type: CENCEL_BOOKING, payload: response});
    }
  } catch (error) {
    console.log(error);
    dispatch({type: CENCEL_BOOKING, payload: {response: error}});
  }
};

export const fetchCancelledAppointments = userdata => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/bookings/customerCancelAppointmentHistory',
      userdata,
    );

    if (response) {
      console.log("cancelBookings",response);
      
      dispatch({type: CANCEL_BOOKING_LIST, payload: response});
    }
  } catch (error) {
    console.log(error);
    dispatch({type: CANCEL_BOOKING_LIST_ERROR, payload: {response: error}});
  }
};