import {
  FETCH_CUSTOMER_DETAILS,
  EDIT_PROFILE,
  FETCH_TRANSACTION_HISTORY,
  FETCH_APPOINTMENT_HISTORY,
  FETCH_UPCOMMING_APPOINTMENT,
  CENCEL_BOOKING,
  CANCEL_BOOKING_LIST,
  ADD_REVIEW
} from '../../../../redux/actions/types';

const intialState = {
  error: '',
  customerDetails: {},
  transactionHistory: {},
  appointmentHistory: {},
  upcommingAppointment: {},
  editProfileDetails: {},
  cancelBookings: {},
  cancelBookingList: {},
  reviewDetails: {},
};

export default function(state = intialState, action) {
  switch (action.type) {

    case ADD_REVIEW:
        return {
          ...state,
          reviewDetails: action.payload,
          error: action.payload.response,
        };
    
    case FETCH_CUSTOMER_DETAILS:
      return {
        ...state,
        customerDetails: action.payload,
        error: action.payload.response,
      };
      
      case EDIT_PROFILE:
      return {
        ...state,
        editProfileDetails: action.payload,
        error: action.payload.response,
      };

      case FETCH_TRANSACTION_HISTORY:
      return {
        ...state,
        transactionHistory: action.payload,
        error: action.payload.response,
      };

      case FETCH_APPOINTMENT_HISTORY:
      return {
        ...state,
        appointmentHistory: action.payload,
        error: action.payload.response,
      };

      case FETCH_UPCOMMING_APPOINTMENT:
      return {
        ...state,
        upcommingAppointment: action.payload,
        error: action.payload.response,
      };

      case CENCEL_BOOKING:
      return {
        ...state,
        cancelBookings: action.payload,
        error: action.payload.response,
      };

      case CANCEL_BOOKING_LIST:
      return {
        ...state,
        cancelBookingList: action.payload,
        error: action.payload.response,
      };
    
    default:
      return state;
  }
}
