import {FORGOTPASSWORD, RESET_PASSWORD, CHANGE_PASSWORD} from '../../../../redux/actions/types';

const intialState = {
  forgotPasswordUserDetails: {},
  resetPasswordUserDetails: {},
  changePasswordUserDetails: {},

  error: '',
};

export default function(state = intialState, action) {
  //console.log(action);
  switch (action.type) {
    case FORGOTPASSWORD:
      return {
        ...state,
        forgotPasswordUserDetails: action.payload,
        error: action.payload.response,
      };
    case RESET_PASSWORD:
      return {
        ...state,
        resetPasswordUserDetails: action.payload,
        error: action.payload.response,
      };
      case CHANGE_PASSWORD:
      return {
        ...state,
        changePasswordUserDetails: action.payload,
        error: action.payload.response,
      };
    default:
      return state;
  }
}
