import {FORGOTPASSWORD, RESET_PASSWORD, CHANGE_PASSWORD} from '../../../../redux/actions/types';
import Api from '../../../../Api/Api';

export const forgotPassword = userDetails => async dispatch => {
  try {
    const response = await Api.postApi('users/appforgetpassword', userDetails);
    console.log({response});
    if (response) {
      dispatch({type: FORGOTPASSWORD, payload: {...response}});
    }
  } catch (error) {
    console.log(error.err);
    dispatch({type: FORGOTPASSWORD, payload: {response: error}});
  }
};


export const resetPassword = userDetails => async dispatch => {
  try {
    const response = await Api.postApi('users/appresetpassword', userDetails);
    console.log({response});
    if (response) {
      dispatch({type: RESET_PASSWORD, payload: {...response}});
    }
  } catch (error) {
    console.log(error);
    dispatch({type: RESET_PASSWORD, payload: {response: error}});
  }
};

export const changeUserPassword = userDetails => async dispatch => {
  try {
    const response = await Api.postApi('users/changeUserPassword', userDetails);
    console.log({response});
    if (response) {
      dispatch({type: CHANGE_PASSWORD, payload: {...response}});
    }
  } catch (error) {
    console.log(error);
    dispatch({type: CHANGE_PASSWORD, payload: {response: error}});
  }
};
