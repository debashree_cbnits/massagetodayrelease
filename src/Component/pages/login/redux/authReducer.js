import {LOGIN, SIGNUP, FBLOGIN, GOOGLElOGIN} from '../../../../redux/actions/types';

const intialState = {
  userDetails: {},
  signUpDetails: {},
  fbLoginn: {},
  isLoggedIn: '',
  error: '',
};

export default function(state = intialState, action) {
  //console.log(action);
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        userDetails: action.payload,
        isLoggedIn: action.payload.isLoggedIn,
        error: action.payload.response,
        // login_userid: action.payload.userId.toString(),
      };
    case SIGNUP:
      return {
        ...state,
        signUpDetails: action.payload,
        error: action.payload.response,
      };

    case FBLOGIN:
      return {
        ...state,
        userDetails: action.payload,
        isLoggedIn: action.payload.isLoggedIn,
        error: action.payload.response,
      };
      case GOOGLElOGIN:
        return {
          ...state,
          userDetails: action.payload,
          isLoggedIn: action.payload.isLoggedIn,
          error: action.payload.response,
        };
    default:
      return state;
  }
}
