import React, {Component} from 'react';
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  ImageBackground,
  Image,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './style';
import Spinner from 'react-native-loading-spinner-overlay';
import {signUp} from './redux/authAction';
import {connect} from 'react-redux';
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter,
} from 'react-native-popup-dialog';

class SignupScreen extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      firstname: '',
      firstname_error: '',
      lastname: '',
      lastname_error: '',
      email: '',
      email_error: '',
      password: '',
      password_error: '',
      spinner: false,
      visible1: false,
      popupMsg: '',
      pin: '',
    };
  }

  firstnameHandler = firstname => {
    this.setState({firstname: firstname});
    let reg = /^[a-zA-Z ]{2,30}$/;
    if (!reg.test(firstname)) {
      this.setState({
        firstname: firstname,
        firstname_error: 'Enter valid firstname',
      });
    } else {
      this.setState({
        firstname: firstname,
        firstname_error: '',
      });
    }
  };

  lastnameHandler = lastname => {
    this.setState({lastname: lastname});
    let reg = /^[a-zA-Z ]{2,30}$/;
    if (!reg.test(lastname)) {
      this.setState({
        lastname: lastname,
        lastname_error: 'Enter valid lastname',
      });
    } else {
      this.setState({
        lastname: lastname,
        lastname_error: '',
      });
    }
  };

  emailHandler = email => {
    this.setState({email: email});
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!reg.test(email)) {
      this.setState({
        email_error: 'Enter valid email id',
      });
    } else {
      this.setState({
        email_error: '',
      });
    }
  };

  passwordHandler = password => {
    this.setState({password: password});
    console.log(this.state.password.length);
    if (this.state.password.length < 5) {
      this.setState({password_error: 'Password must be atleast 6 character'});
    } else {
      this.setState({password_error: ''});
    }
  };

  //--CLOSE POPUP BOX--//
  closePopupbox = () => {
    console.log(this.state.pin);

    this.setState({
      visible1: false,
      pin : 1
    });

    

    // if(this.state.pin === 1 ){
    // this.props.navigation.navigate('Login');
    // }
  };

  handleSubmit = () => {
    if (this.state.firstname == '') {
      this.setState({
        firstname_error: 'Enter firstname',
      });
    }
    if (this.state.lastname == '') {
      this.setState({
        lastname_error: 'Enter lastname',
      });
    }
    if (this.state.email == '') {
      this.setState({
        email_error: 'Enter email id',
      });
    }
    if (this.state.password == '') {
      this.setState({
        password_error: 'Enter password',
      });
    } else {
      this.setState({spinner: true});
      let userDetails = new FormData();
      userDetails.append('first_name', this.state.firstname);
      userDetails.append('last_name', this.state.lastname);
      userDetails.append('email', this.state.email);
      userDetails.append('password', this.state.password),
      this.props.signUp(userDetails);
    }
  };
  componentDidUpdate(prevProps) {
    console.log('within componentdidupdate', prevProps);
    console.log("componentdidupdate",this.state.visible1);
    
    if(this.state.visible1 === false && this.state.pin === 1){
      this.props.navigation.navigate('Login');
      }
    if (prevProps !== this.props) {
      const addDetails = this.props.signUpDetails;
        if (this.props.error && this.props.error !== undefined) {
          if (this.state.visible1 === false) {
            this.setState({visible1: true, popupMsg: addDetails.msg});
          }
        this.setState({spinner: false});
      } else {
        if (addDetails && addDetails !== undefined) {
          if (addDetails.ack === 1) {
            this.setState({
              //pin : 1,
              spinner: false,

            })
            // this.setState({
            // });
            if (this.state.visible1 === false) {
              this.setState({visible1: true, popupMsg: addDetails.msg});
            }
          } else {
            this.setState({
              spinner: false,
            });
          }
        }
      }
    }
  }

  render() {
    return (
      <SafeAreaView>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        <ImageBackground
          source={require('../../assets/images/mainbg.jpg')}
          style={styles.bodymain}>
          <KeyboardAvoidingView enabled behavior="padding">
            <ScrollView>
              <View style={styles.container}>
                <Spinner
                  visible={this.state.spinner}
                  textContent={'Loading...'}
                  textStyle={styles.spinnerTextStyle}
                />
                <TouchableOpacity style={styles.logo}>
                  <Image
                    source={require('../../assets/images/logo.png')}
                    resizeMode="contain"
                    style={{width: 280, height: 80}}
                  />
                </TouchableOpacity>

                <View style={styles.mainform}>
                  <Text style={styles.heading}>
                    Sign Up for quick reservations
                  </Text>
                  <TextInput
                    style={styles.forminput}
                    placeholder="First Name"
                    placeholderTextColor="#9590a9"
                    onChangeText={firstname => this.firstnameHandler(firstname)}
                  />
                  <Text style={styles.error}>{this.state.firstname_error}</Text>

                  <TextInput
                    style={styles.forminput}
                    placeholder="Last Name"
                    placeholderTextColor="#9590a9"
                    onChangeText={lastname => this.lastnameHandler(lastname)}
                  />
                  <Text style={styles.error}>{this.state.lastname_error}</Text>

                  <TextInput
                    style={styles.forminput}
                    placeholder="Email"
                    placeholderTextColor="#9590a9"
                    onChangeText={email => this.emailHandler(email)}
                  />
                  <Text style={styles.error}>{this.state.email_error}</Text>

                  <TextInput
                    style={styles.forminput}
                    placeholder="Password"
                    placeholderTextColor="#9590a9"
                    onChangeText={password => this.passwordHandler(password)}
                    secureTextEntry={true}
                  />
                  <Text style={styles.error}>{this.state.password_error}</Text>

                  {/* <TouchableOpacity style={styles.forgetpass}>
                    <Text style={styles.pinktext}>Forgot Password</Text>
                  </TouchableOpacity> */}
                  <TouchableOpacity
                    style={styles.pinkbtn}
                    onPress={() => this.handleSubmit()}>
                    <Text style={styles.btntext}>SIGN UP</Text>
                  </TouchableOpacity>
                  <View style={styles.container}>
                    <Dialog
                      visible={this.state.visible1}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: 'bottom',
                        })
                      }
                      onTouchOutside={() => {
                        this.closePopupbox()
                      }}
                      dialogStyle={{width: '80%'}}
                      footer={
                        <DialogFooter>
                          <DialogButton
                            textStyle={{
                              fontSize: 14,
                              color: '#333',
                              fontWeight: '700',
                            }}
                            text="OK"
                            onPress={() => {
                              this.closePopupbox()
                            }}
                          />
                        </DialogFooter>
                      }>
                      <DialogContent>
                        <Text style={styles.popupText}>
                          {this.state.popupMsg}
                        </Text>
                      </DialogContent>
                    </Dialog>
                  </View>
                  <Image
                    source={require('../../assets/images/or.png')}
                    resizeMode="contain"
                    style={styles.orline}
                  />
                  
                  {/* <View style={styles.jsbtn}>
                    <TouchableOpacity style={styles.googlebtn}>
                      <Text style={styles.btntext}>
                        <Icon name="google" style={{marginRight: 10}} /> With
                        GOOGLE
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.fbbtn}>
                      <Text style={styles.btntext}>
                        {' '}
                        <Icon name="facebook" style={{marginRight: 10}} /> With
                        FACEBOOK
                      </Text>
                    </TouchableOpacity>
                  </View> */}
                  <View style={styles.bttext}>
                    <Text style={styles.text}>
                      Already Member Massaged Today? 
                    </Text>
                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate('Login')}>
                      <Text style={styles.pinktext}> Login</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.auth,
});

export default connect(
  mapStateToProps,
  {signUp},
)(SignupScreen);
