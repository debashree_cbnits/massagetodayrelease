import React, { Component } from 'react'
import { Text, View, StatusBar, SafeAreaView, Image, TouchableOpacity } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import styles from './style'

export default class Thankyou extends Component {
  static navigationOptions = {
    header: null
  }

  

  render() {
    //const { goBack } = this.props.navigation;
    return (
      <SafeAreaView style={{ backgroundColor: '#303038', paddingTop:30}}>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
            {/* <View style={[styles.topbar,]}>               
                <TouchableOpacity onPress={() => goBack()} >
                    <Icon name="md-arrow-back" type='ionicon' color="#fff" />
                </TouchableOpacity>
            </View> */}
            <View style={styles.graycontainer}>
                <View style={styles.thankyou}>
                    <Image source={require('../../assets/images/thank-you.png')} resizeMode="contain" style={{width:'100%', height:'100%'}} />
                </View>
            </View>
      </SafeAreaView>
    )
  };
}


