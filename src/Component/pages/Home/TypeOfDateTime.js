import React, { Component } from "react";
import { Text, View, TouchableOpacity, Image } from "react-native";
import styles from "./style";
import Api from "../../../Api/Api";
import CalendarPicker from "react-native-calendar-picker";
import moment from "moment";
import { connect } from "react-redux";
import { fetchServices } from "./redux/spaAction";

class TypeOfDateTime extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      checked: "",
      slotTime: "",
      timeStatus: "",
      holidayComment: "",
      timing: [],
      servicedate: "",
      servicetime: "",
      timingMsg: "",
      itemNo: "",
      pressStatus: "0",
      calender_error: "",
      calender_state: 1,
      timeerror: "",
      timecheck : false
    };
  }

  //--select service date and fetch timeslot acording to the date--//
  onDateChange = date => {
    todayDate = moment(new Date()).format("DD/MM/YYYY");
    console.log("todayDate", todayDate);
    var dat = todayDate.split("/");
    console.log("dddddd", dat);
    var dat_day = dat[0];
    var dat_mnth = dat[1];
    var dat_yr = dat[2];

    this.setState({
      calender_error: ""
    });

    var increaseMonth = date._i.month + 1;

    let service_date = increaseMonth + "/" + date._i.day + "/" + date._i.year;
    let check_date = date._i.day + "/" + increaseMonth + "/" + date._i.year;
    console.log("selected date in typeof component page", check_date);
    if(dat_mnth == increaseMonth &&
      dat_day == date._i.day &&
      dat_yr == date._i.year){
        this.setState({timecheck : true})
      }
      else{
        this.setState({timecheck : false})
      }

    if (dat_yr > date._i.year) {
      console.log("yr");
      this.setState({
        calender_error:
          "Booking can not be done in past date. Enter a valid date",
        calender_state: ""
      });
    } else if (dat_mnth > increaseMonth && dat_yr == date._i.year) {
      console.log("mnth");
      this.setState({
        calender_error:
          "Booking can not be done in past date. Enter a valid date",
        calender_state: ""
      });
    } else if (
      dat_mnth == increaseMonth &&
      dat_day > date._i.day &&
      dat_yr == date._i.year
    ) {
      console.log("day");
      this.setState({
        calender_error:
          "Booking can not be done in past date. Enter a valid date",
        calender_state: ""
      });
    } else {
      console.log("else date");

      this.setState({
        servicedate: service_date,
        calender_state: 1
      });
      console.log("selected check_date", check_date);
      console.log("selected todayDate", todayDate);
      let spaIdDate = new FormData();
      spaIdDate.append("spa_id", this.props.selectedSpaDetails.spaId);
      spaIdDate.append("date", service_date);
      console.log("spaIdDate", spaIdDate);

      Api.postApi("webservice/users/get_spa_availability", spaIdDate)
        .then(spatimimg => {
          if (spatimimg.msg == "Spa is not available for this day!") {
            this.setState({
              timeStatus: "",
              timingMsg: spatimimg.msg,
              holidayComment: ""
            });
          } else if (spatimimg.msg == "It is a holiday!") {
            this.setState({
              timeStatus: "",
              timingMsg: spatimimg.msg,
              holidayComment: spatimimg.comment
            });
          } else {
            this.setState({ timeStatus: "timeslot available" });
          }
          if (spatimimg.Ack == 1) {
            console.log("timing fetched", spatimimg);

            //calculating time slots acording to the start and end time//
            var mins = moment
              .utc(
                moment(spatimimg.info.available_end_time, "HH:mm:ss").diff(
                  moment(spatimimg.info.available_start_time, "HH:mm:ss")
                )
              )
              .format("mm");
            var start_moment = moment.utc(
              moment(spatimimg.info.available_start_time, "HH:mm:ss")
            );
            var end_moment = moment.utc(
              moment(spatimimg.info.available_end_time, "HH:mm:ss")
            );
            var diff = moment.duration(end_moment.diff(start_moment));
            var duration_in_half_an_hour = Math.round(
              diff._milliseconds / (1000 * 60 * 15)
            );
            this.setState({
              slotTime: duration_in_half_an_hour
            });

            let timeed = [];
            let addHour = 15;
            for (let i = 0; i < this.state.slotTime; i++) {
              if (i == 0) {
                timeed[i] = moment(
                  spatimimg.info.available_start_time,
                  "HH:mm"
                ).format("HH:mm");
              } else {
                timeed[i] = moment(spatimimg.info.available_start_time, "HH:mm")
                  .add(addHour, "minutes")
                  .format("HH:mm");
                addHour += 15;
              }
            }
            console.log("selected time", timeed);

            this.setState({ timing: timeed });
          } else {
            console.log("Nope");
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
  };

  //--select time slot--//
  selectBox = (id, time) => {
    console.log(this.state.timecheck);
    this.setState({timeerror : ""})

    if(this.state.timecheck == true){

     var today = new Date();
     var currenttime = today.getHours();

    console.log("selectbox", currenttime);
    var hr = time.split(":");
    var hours = hr[0];
    console.log("dddddd", hours);

    if(hours > currenttime) {
      console.log("done");
      
      this.setState({
        itemNo: id,
        pressStatus: "1",
        servicetime: time
      });
  
      let sendDateTime = {
        date: this.state.servicedate,
        time: time
      };
      console.log("sendDateTime", sendDateTime);
      this.props.onSelectDateTime(sendDateTime);
    }
    else{
      console.log("no");
      this.setState({
        itemNo: "",
        pressStatus: "",
        servicetime: ""
      });
      this.setState({timeerror : "Booking can not be done. Select time properly."})
    }
    }
    else{
      this.setState({
        itemNo: id,
        pressStatus: "1",
        servicetime: time
      });
  
      let sendDateTime = {
        date: this.state.servicedate,
        time: time
      };
      console.log("sendDateTime", sendDateTime);
      this.props.onSelectDateTime(sendDateTime);
    }
    
    
    
  };

  render() {
    const { checked } = this.state;
    const changeStyle =
      this.state.pressStatus === "0"
        ? [styles.greycard, styles.margingray, styles.active]
        : [styles.whitecard, styles.margingray, styles.active];
    const StaticStyle = [styles.graycard, styles.margingray, styles.active];

    return (
      <View>
        <View style={{ backgroundColor: "#fff", padding: 10 }}>
          <CalendarPicker onDateChange={this.onDateChange} />
          <Text style={styles.calender_error}>{this.state.calender_error}</Text>
        </View>
        {this.state.calender_state ? (
          <View>
            {this.state.timeStatus ? (
              <View>
                <Text style={styles.h1}>Choose Time Slot</Text>

                <View style={styles.flexwrap}>
                  {this.state.timing.map((item, key) => (
                    <TouchableOpacity
                      style={
                        this.state.itemNo === key ? changeStyle : StaticStyle
                      }
                      onPress={() => this.selectBox(key, item)}
                      value={item}
                    >
                      <Image
                        source={require("../../assets/images/clock.png")}
                        resizeMode="contain"
                        style={{ width: 20, height: 20 }}
                      />
                      <Text style={styles.grtext}>{item}</Text>
                    </TouchableOpacity>
                  ))}
                </View>
                <Text style={styles.calender_error}>{this.state.timeerror}</Text>
              </View>
            ) : (
              <View>
                <Text style={styles.h1}>{this.state.timingMsg}</Text>
                <Text style={styles.h1}>{this.state.holidayComment}</Text>
              </View>
            )}
          </View>
        ) : null}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  ...state.spa
});
export default connect(mapStateToProps, { fetchServices })(TypeOfDateTime);
