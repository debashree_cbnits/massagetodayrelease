import React, {Component} from 'react';
import {Text, View, Image, Button, Alert} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import Api from '../../../Api/Api';

export default class imageUpload extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      photo: null,
      firstName: 'Tina',
      lastName: 'Ghosh',
      userId: '19',
      email: 'sharmistha.ghosh@cbnits.com',
      ph: '12345678',
      address: 'agdhghdsgh',
      ImageSource: '',
    };
  }

  handleImageUpload = () => {
    // const option = {
    //   noData: true,
    // };
    // ImagePicker.launchImageLibrary(option, response => {
    //   console.log('response image', response);
    //   if (response.uri) {
    //     this.setState({
    //       photo: response,
    //     });
    //   }
    // });
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500
    };
    ImagePicker.showImagePicker(options, (showImage) => {
      console.log("showImage",showImage);
      
      if (showImage.didCancel) {
      }
      else if (showImage.error) {
      }
      else if (showImage.customButton) {
      }
      else {
       // let source = { uri: showImage.data };
      //  let file = showImage.fileName
         this.setState({
           ImageSource: showImage.data,
           photo: showImage.uri,
         });
      }
    })
  }

  handleImageSend = () => {
    let photoUpload = new FormData();
    photoUpload.append('first_name', this.state.firstName);
    photoUpload.append('last_name', this.state.lastName);
    photoUpload.append('email', this.state.email);
    photoUpload.append('phone', this.state.ph);
    photoUpload.append('address', this.state.address);
    photoUpload.append('profile_image', this.state.ImageSource);
    photoUpload.append('id', this.state.userId);
    console.log('photoUpload', photoUpload);
    Api.postApi('webservice/users/userUpdateProfile', photoUpload)
      .then(response => {
        console.log(response);

        if (response.Ack == 1) {
          Alert.alert('ack1', response.msg);
        } else {
          Alert.alert('not ack1', response.msg);
        }
      })
      .catch(err => {
        console.log(err);
        Alert.alert('err', err);
      });
  };

  render() {
    const {photo} = this.state;
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        {photo && (
          <Image source={{uri: this.state.photo}} style={{width: 300, height: 300}} />
        )}
        <Button title="Choose photo" onPress={this.handleImageUpload} />
        <Button title="Send photo" onPress={this.handleImageSend} />
      </View>
    );
  }
}
