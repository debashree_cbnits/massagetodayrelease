import React, { PureComponent } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Alert,
  StatusBar,
  SafeAreaView,
  TouchableOpacity
} from "react-native";
import Spoiler from "../components/Spoiler";
import Button from "../components/Button";
import { connect } from "react-redux";
import { adminPayment } from "./redux/spaAction";
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter
} from "react-native-popup-dialog";
import Spinner from "react-native-loading-spinner-overlay";
import { Icon, CheckBox } from "react-native-elements";
import styles from "./style";
import { ScrollView } from "react-native-gesture-handler";

class CardFormScreen extends PureComponent {
  static title = "Card Form";

  state = {
    number: "",
    month: "",
    year: "",
    cvv: "",
    visible1: false,
    popupMsg: "",
    error: "",
    spinner: false,
    paybutton: 1,

    customer_id:
      this.props.navigation.state.params &&
      this.props.navigation.state.params.customer_id
        ? this.props.navigation.state.params.customer_id
        : "",
    masseuse_id:
      this.props.navigation.state.params &&
      this.props.navigation.state.params.masseuse_id
        ? this.props.navigation.state.params.masseuse_id
        : "",
    spa_id:
      this.props.navigation.state.params &&
      this.props.navigation.state.params.spa_id
        ? this.props.navigation.state.params.spa_id
        : "",
    booking_date:
      this.props.navigation.state.params &&
      this.props.navigation.state.params.booking_date
        ? this.props.navigation.state.params.booking_date
        : "",
    booking_start_time:
      this.props.navigation.state.params &&
      this.props.navigation.state.params.booking_start_time
        ? this.props.navigation.state.params.booking_start_time
        : "",
    arr_service_ids:
      this.props.navigation.state.params &&
      this.props.navigation.state.params.arr_service_ids
        ? this.props.navigation.state.params.arr_service_ids
        : "",
    arr_service_names:
      this.props.navigation.state.params &&
      this.props.navigation.state.params.arr_service_names
        ? this.props.navigation.state.params.arr_service_names
        : "",
    arr_service_durations:
      this.props.navigation.state.params &&
      this.props.navigation.state.params.arr_service_durations
        ? this.props.navigation.state.params.arr_service_durations
        : "",
    arr_service_prices:
      this.props.navigation.state.params &&
      this.props.navigation.state.params.arr_service_prices
        ? this.props.navigation.state.params.arr_service_prices
        : "",
    service_tips:
      this.props.navigation.state.params &&
      this.props.navigation.state.params.service_tips
        ? this.props.navigation.state.params.service_tips
        : "",
    discount:
      this.props.navigation.state.params &&
      this.props.navigation.state.params.discount
        ? this.props.navigation.state.params.discount
        : "",
    admin_payment_amount:
      this.props.navigation.state.params &&
      this.props.navigation.state.params.admin_payment_amount
        ? this.props.navigation.state.params.admin_payment_amount
        : "",
    final_amount:
      this.props.navigation.state.params &&
      this.props.navigation.state.params.final_amount
        ? this.props.navigation.state.params.final_amount
        : "",
    adminBookingFees:
      this.props.navigation.state.params &&
      this.props.navigation.state.params.adminBookingFees
        ? this.props.navigation.state.params.adminBookingFees
        : ""
  };

  componentDidUpdate(prevProps) {
    console.log(this.props);

    if (prevProps.adminPaymentResponse !== this.props.adminPaymentResponse) {
      this.setState({ spinner: false });
      console.log("if2");

      const addDetails = this.props.adminPaymentResponse;
      if (this.props.error && this.props.error !== undefined) {
        console.log("if2");
        Alert.alert("Payment Not Successfull. Something went wrong.");

        // if (this.state.visible1 === false) {
        //   this.setState({visible1: true, popupMsg: addDetails.msg});
        // }
        //this.setState({spinner: false});
      } else {
        console.log("else1");

        if (addDetails.Ack === 1) {
          this.setState({ paybutton: 2 });
          this.props.navigation.navigate("Thankyou");
        } else {
          console.log("else2");

          // this.setState({
          //   spinner: false,
          // });
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: "Payment Not Successfull. Something went wrong."
            });
          }
          // Alert.alert('Payment Not Successfull. Something went wrong.');
        }
      }
    }
  }

  //--CLOSE POPUP BOX--//
  closePopupbox = () => {
    console.log("visible value", this.state.visible1);

    if (this.state.visible1 == true) {
      this.setState({ visible1: false });
    }
  };

  handleCustomPayPress = async () => {
    console.log(
      "handleCardPayPress",
      this.props.navigation.state.params.booking_id
    );
    var booking_id = this.props.navigation.state.params.booking_id;

    if (this.state.number === "") {
      this.setState({
        error: "Enter Card Number"
      });
    } else if (this.state.month === "") {
      this.setState({
        error: "Enter Expair month"
      });
    } else if (this.state.year === "") {
      this.setState({
        error: "Enter Expair Year"
      });
    } else if (this.state.cvv === "") {
      this.setState({
        error: "Enter Valid CVV"
      });
    } else {
      this.setState({ spinner: true });
      let adminPayment = new FormData();
      adminPayment.append("card_number", this.state.number);
      adminPayment.append("exp_mnth", this.state.month);
      adminPayment.append("exp_yr", this.state.year);
      adminPayment.append("cvv", this.state.cvv);

      adminPayment.append("customer_id", this.state.customer_id);
      adminPayment.append("masseuse_id", this.state.masseuse_id);
      adminPayment.append("spa_id", this.state.spa_id);
      adminPayment.append("booking_date", this.state.booking_date);
      adminPayment.append("booking_start_time", this.state.booking_start_time);
      adminPayment.append("arr_service_ids", this.state.arr_service_ids);
      adminPayment.append("arr_service_names", this.state.arr_service_names);
      adminPayment.append(
        "arr_service_durations",
        this.state.arr_service_durations
      );
      adminPayment.append("arr_service_prices", this.state.arr_service_prices);
      adminPayment.append("service_tips", this.state.service_tips);
      adminPayment.append("discount", this.state.discount);
      adminPayment.append(
        "admin_payment_amount",
        this.state.admin_payment_amount
      );
      adminPayment.append("final_amount", this.state.final_amount);

      console.log("adminPaymentwwwwww", adminPayment);
      this.props.adminPayment(adminPayment);
    }
  };

  namechange = (text, type) => {
    if (type == "number")
      this.setState({
        number: text,
        error: ""
      });
    if (type == "month")
      this.setState({
        month: text,
        error: ""
      });
    if (type == "year")
      this.setState({
        year: text,
        error: ""
      });
    if (type == "cvv")
      this.setState({
        cvv: text,
        error: ""
      });
  };

  render() {
    console.log("props at cardform", this.props);
    console.log("props at cardform", this.state);

    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        <View style={[styles.justifyrow, { marginLeft: 20, marginTop: -20 }]}>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate("booking", {
                send_masseuseId: this.state.masseuse_id,
                adminBookingFees: this.state.adminBookingFees
              })
            }
          >
            <Icon name="md-arrow-back" type="ionicon" color="#fff" />
          </TouchableOpacity>
        </View>
        <ScrollView>
          <View style={[styles.container1, { marginTop: 40 }]}>
            <Spoiler style={{ width: 300 }} title="Enter Your Card Details">
              <View style={styles.params}>
                <View
                  style={{
                    flexDirection: "row",
                    marginBottom: 10,
                    marginRight: 20
                  }}
                >
                  <Text style={styles.param}>Number:</Text>
                  <TextInput
                    style={{
                      height: 45,
                      width: 180,
                      borderColor: "gray",
                      borderWidth: 1,
                      borderRadius: 5,
                      marginLeft: 20,
                      padding: 8,
                      fontSize: 15,
                      textAlign: "center",
                      color: "#000000"
                    }}
                    keyboardType={"numeric"}
                    value={this.state.number}
                    onChangeText={text => this.namechange(text, "number")}
                  />
                </View>
                <View style={{ flexDirection: "row", marginBottom: 10 }}>
                  <Text style={styles.param}>Month:</Text>
                  <TextInput
                    style={{
                      height: 45,
                      width: 60,
                      borderColor: "gray",
                      borderWidth: 1,
                      borderRadius: 5,
                      marginLeft: 33,
                      padding: 8,
                      fontSize: 15,
                      textAlign: "center",
                      color: "#000000"
                    }}
                    keyboardType={"numeric"}
                    value={this.state.month}
                    onChangeText={text => this.namechange(text, "month")}
                  />
                </View>
                <View style={{ flexDirection: "row", marginBottom: 10 }}>
                  <Text style={styles.param}>Year:</Text>
                  <TextInput
                    style={{
                      height: 45,
                      width: 60,
                      borderColor: "gray",
                      borderWidth: 1,
                      borderRadius: 5,
                      marginLeft: 50,
                      padding: 8,
                      fontSize: 15,
                      textAlign: "center",
                      color: "#000000"
                    }}
                    keyboardType={"numeric"}
                    value={this.state.year}
                    onChangeText={text => this.namechange(text, "year")}
                  />
                </View>
                <View style={{ flexDirection: "row", marginBottom: 10 }}>
                  <Text style={styles.param}>CVV:</Text>
                  <TextInput
                    style={{
                      height: 45,
                      width: 60,
                      borderColor: "gray",
                      borderWidth: 1,
                      borderRadius: 5,
                      marginLeft: 50,
                      padding: 8,
                      fontSize: 15,
                      textAlign: "center",
                      color: "#000000"
                    }}
                    keyboardType={"numeric"}
                    value={this.state.cvv}
                    onChangeText={text => this.namechange(text, "cvv")}
                  />
                </View>
              </View>
            </Spoiler>
            <Spinner
              visible={this.state.spinner}
              textContent={"Loading..."}
              textStyle={styles.spinnerTextStyle}
            />
            <View style={styles.container}>
              <Dialog
                visible={this.state.visible1}
                dialogAnimation={
                  new SlideAnimation({
                    slideFrom: "bottom"
                  })
                }
                onTouchOutside={() => {
                  this.closePopupbox();
                }}
                dialogStyle={{ width: "80%" }}
                footer={
                  <DialogFooter>
                    <DialogButton
                      textStyle={{
                        fontSize: 14,
                        color: "#333",
                        fontWeight: "700"
                      }}
                      text="OK"
                      onPress={() => {
                        this.closePopupbox();
                      }}
                    />
                  </DialogFooter>
                }
              >
                <DialogContent>
                  <Text style={styles.popupText}>{this.state.popupMsg}</Text>
                </DialogContent>
              </Dialog>
            </View>
            <Text style={styles.error}>{this.state.error}</Text>
            {this.state.paybutton == 1 ? (
              <Button
                style={styles.pinkbtn1}
                text="Pay"
                onPress={this.handleCustomPayPress}
              />
            ) : (
              <Button style={styles.pinkbtndisable} text="Pay" />
            )}
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.spa
});
export default connect(mapStateToProps, { adminPayment })(CardFormScreen);
