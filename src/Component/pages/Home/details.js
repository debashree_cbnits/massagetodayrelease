import React, {Component} from 'react';
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';
import _ from 'lodash';
import {Icon} from 'react-native-elements';
import styles from './style';
import {connect} from 'react-redux';
import {fetchSpaDetails} from './redux/spaAction';
import Ammenities from './Ammenities';
import SpaImage from './SpaImage';
import Service from './Service';
import Review from './Review';

import MapView, {Marker} from 'react-native-maps';

class Details extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      SpaId: '',
      content: [],
      image: [],
      service: [],
      review: [],
      count: '',
      spaDetails: {},
      spaName: '',
      spaDescription: '',
      spaAddress: '',
      spaNoOfBeds: '',
      spaParkingPricePerHour: '',
      spaIsParkingAvailable: '',
      spaWifyAvailability: '',
      spaRating: '',
      location: {
        latitude: '',
        longitude: '',
        latitudeDelta: '',
        longitudeDelta: '',
      },
    };
    this.apiCall();
  }

  apiCall = () => {
    console.log('apiCall');

    let userDetails = new FormData();
    userDetails.append('id', this.props.selectedSpaDetails.spaId);
    this.props.fetchSpaDetails(userDetails);
  };

  componentWillReceiveProps(nextProps) {
    console.log('componentWillReceiveProps', nextProps);
    this.setState({
      spaName: nextProps.spaDetails.spa_details.name,
      spaDescription: nextProps.spaDetails.spa_details.description,
      spaAddress: nextProps.spaDetails.spa_details.address,
      count: nextProps.spaDetails.count,
      spaNoOfBeds: nextProps.spaDetails.spa_details.no_of_bed,
      spaParkingPricePerHour:
        nextProps.spaDetails.spa_details.parking_price_per_hour,
      spaIsParkingAvailable:
        nextProps.spaDetails.spa_details.is_parking_available,
      spaWifyAvailability: nextProps.spaDetails.spa_details.is_wifi_available,
      spaAirconditionAvailable:
        nextProps.spaDetails.spa_details.is_aircondition_available,
      spaRating: nextProps.spaDetails.spa_details.rating,
      image: nextProps.spaDetails.image,
      //service: nextProps.spaDetails.service,
      review: nextProps.spaDetails.review,
      location: {
        latitude: Number(nextProps.spaDetails.spa_details.lattitude),
        longitude: Number(nextProps.spaDetails.spa_details.longitude),
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
    });
    //this.setState(nextProps);

    var new_service = [];

    var serviceArray = nextProps.spaDetails.service;

    console.log('serviceArray', serviceArray);

    for (var k = 0; k < serviceArray.length; k++) {
      var price_duration = [];
      price_duration.push({
        price: serviceArray[k].price,
        duration: serviceArray[k].duration,
      });
      new_service.push({
        id: k,
        service_id: serviceArray[k].service_id,
        service_name: serviceArray[k].service_name,
        price_duration: price_duration,
      });
    }

    console.log('est', new_service);

    var test = [];

    for (var i = 0; i < new_service.length; i++) {
      if (test.length) {
        for (var j = 0; j < test.length; j++) {
          var unmatch = 1;
          if (test[j].service_id == new_service[i].service_id) {
            unmatch = 0;

            var test1 = test[j].price_duration;
            test1.push({
              price: new_service[i].price_duration[0].price,
              duration: new_service[i].price_duration[0].duration,
            });
            test[j].price_duration = test1;
          }
        }
        if (unmatch == 1) {
          test.push(new_service[i]);
        }
      } else {
        test.push(new_service[i]);
      }
    }
    console.log('est', test);
    this.setState({service: test})
  }

  render() {
    console.log('render details props', this.props);
    console.log('render details states location', this.states);

    const {goBack} = this.props.navigation;

    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        <View style={[styles.topbar]}>
          <TouchableOpacity onPress={() => goBack()}>
            <Icon name="md-arrow-back" type="ionicon" color="#fff" />
          </TouchableOpacity>
          {/* <TouchableOpacity style={styles.notify}>
            <Image
              source={require('../../assets/images/bell.png')}
              resizeMode="contain"
              style={{width: 40, height: 35}}
            />
          </TouchableOpacity> */}
        </View>

        {this.state.spaName ? (
          <ScrollView>
            <SpaImage
              image={this.state.image}
              spaName={this.state.spaName}
              spaAddress={this.state.spaAddress}
              spaRating={this.state.spaRating}
            />
            <View style={styles.graycontainer}>
              <Ammenities
                spaDescription={this.state.spaDescription}
                count={this.state.count}
                spaNoOfBeds={this.state.spaNoOfBeds}
                spaIsParkingAvailable={this.state.spaIsParkingAvailable}
                spaParkingPricePerHour={this.state.spaParkingPricePerHour}
                spaWifyAvailability={this.state.spaWifyAvailability}
                spaAirconditionAvailable={this.state.spaAirconditionAvailable}
              />

              <View style={styles.graycard}>
                <View
                  style={[
                    styles.place,
                    {paddingLeft: 15, paddingRight: 15, marginBottom: 15},
                  ]}>
                  <Image
                    source={require('../../assets/images/marker.png')}
                    resizeMode="contain"
                    style={{width: 24, height: 30, marginRight: 10}}
                  />
                  <Text numberOfLines={5} style={styles.placetext}>
                    {this.state.spaAddress}
                  </Text>
                </View>
                <View style={styles.maphold}>
                  {this.state.location.latitude ? (
                    <MapView
                      style={{width: '100%', height: 200}}
                      initialRegion={this.state.location}>
                      <Marker coordinate={this.state.location} />
                    </MapView>
                  ) : null}
                </View>
              </View>

              <Service service={this.state.service} />
              <Review
                review={this.state.review}
                spaRating={this.state.spaRating}
                // wifi={this.state.spaWifyAvailability}
                // carParking={this.state.spaIsParkingAvailable}
                // parkingPrice={this.state.spaParkingPricePerHour}
              />
              <TouchableOpacity
                style={styles.pinkbtn}
                onPress={() => this.props.navigation.navigate('Typeof',{service: this.state.service})}>
                <Text style={styles.btntext}>Continue to Booking</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        ) : null}
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.spa,
});

export default connect(mapStateToProps, {fetchSpaDetails})(Details);
