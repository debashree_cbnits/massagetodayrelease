import {
  FETCH_SPA_DETAILS,
  MASSEUSE_PER_SPA,
  SELECTED_SPA,
  SELECTED_SERVICE,
  FETCH_SERVICES,
  FINAL_BOOKING,
  SPA_DETAILS_LIST,
  ADMIN_PAYMENT_DETAILS,
  SERVICES,
  ADMIN_BOOKING_FEES
} from '../../../../redux/actions/types';
import Api from '../../../../Api/Api';



export const spaDetailsList = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/spasDetailsData',
      userDetails,
    );
    console.log('-----', response);
    if (response) {
      dispatch({type: SPA_DETAILS_LIST, payload: response});
    }
  } catch (error) {
    console.log(error);
    dispatch({type: SPA_DETAILS_LIST, payload: {response: error}});
  }
};

export const fetchSpaDetails = userDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/getSpaDetails',
      userDetails,
    );

    if (response) {
      dispatch({
        type: FETCH_SPA_DETAILS,
        payload: {
          spa_details: response.spa_details,
          image: response.image,
          service: response.service,
          review: response.review,
          count: response.count,
        },
      });
      //dispatch({type: FETCH_SPA_DETAILS, payload:response.spa_details});
    }
  } catch (error) {
    console.log(error);
    dispatch({type: FETCH_SPA_DETAILS, payload: {response: error}});
  }
};

// export const fetchMasseuse = details => async dispatch => {
//   try {
//     const response = await Api.postApi(
//       'webservice/users/masseuse_per_spa',
//       details,
//     );
//     console.log(response);
//     if (response) {
//       dispatch({type: FETCH_ALL_MASSEUSE, payload: response});
//     }
//   } catch (error) {
//    // console.log(error);
//     dispatch({type: FETCH_ALL_MASSEUSE, payload: {response: error}});
//   }
// };

export const masseusePerSpa = Details => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/masseuse_per_spa',
      Details,
    );
    console.log('messeuse response', response);
    if (response) {
      dispatch({type: MASSEUSE_PER_SPA, payload: response.masseuse_per_spa});
    }
  } catch (error) {
    console.log(error);
    dispatch({type: MASSEUSE_PER_SPA, payload: {response: error}});
  }
};

export const adminBookingFees = () => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/admin_percentage',""
    );
    console.log('ADMIN response', response);
    if (response) {
      dispatch({type: ADMIN_BOOKING_FEES, payload: response});
    }
  } catch (error) {
    console.log(error,"error");
    dispatch({type: ADMIN_BOOKING_FEES, payload: {response: error}});
  }
};

export const fetchServices = spaDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/users/services_per_spa',
      spaDetails,
    );
    console.log(response);
    if (response) {
      dispatch({type: FETCH_SERVICES, payload: response.services_per_spa});
    }
  } catch (error) {
    console.log(error);
    dispatch({type: FETCH_SERVICES, payload: {response: error}});
  }
};

export const setSelectedSpa = spaDetails => async dispatch => {
  dispatch({type: SELECTED_SPA, payload: {...spaDetails}});
};

export const setSelectedService = serviceDetails => async dispatch => {
  dispatch({type: SELECTED_SERVICE, payload: {...serviceDetails}});
};

export const allServiceNames = services => async dispatch => {
  dispatch({type: SERVICES, payload: {...services}});
};

export const finalBooking = bookingDetails => async dispatch => {
  try {
    const response = await Api.postApi(
      'webservice/bookings/service_booking',
      bookingDetails,
    );
    console.log('booking response', response);
    if (response) {
      dispatch({type: FINAL_BOOKING, payload: response});
    }
  } catch (error) {
    console.log(error);
    dispatch({type: FINAL_BOOKING, payload: {response: error}});
  }
};

export const adminPayment = paymentDetails => async dispatch => {
  console.log('-----sssssssssssssssss');

  try {
    const response = await Api.postApi(
      'webservice/payments/stripeCardTokenCreate',
      paymentDetails,
    );
    console.log('-----sssssssssssssssss', response);
    if (response) {
      console.log('-----sssssssssssssssss', response);

      dispatch({type: ADMIN_PAYMENT_DETAILS, payload: response});
    }
  } catch (error) {
    console.log('-----sssssssssssssssss error', error);

    console.log(error);
    dispatch({type: ADMIN_PAYMENT_DETAILS, payload: {response: error}});
  }
};
