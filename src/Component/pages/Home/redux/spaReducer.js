import {
  FETCH_SPA_DETAILS,
  //FETCH_ALL_MASSEUSE,
  MASSEUSE_PER_SPA,
  SELECTED_SPA,
  SELECTED_SERVICE,
  FETCH_SERVICES,
  FINAL_BOOKING,
  SPA_DETAILS_LIST,
  SERVICES,
  ADMIN_PAYMENT_DETAILS,
  ADMIN_BOOKING_FEES
} from '../../../../redux/actions/types';

const intialState = {
  allMasseuse: [],
  allServices: [],
  error: '',
  selectedSpaDetails: {},
  selectedServiceDetails: {},
  spaDetails: {},
  finalBookingResponse: {},
  spaDiscount: [],
  masseusePerSpa: {},
  servicesNames: {},
  adminPaymentResponse: {},
  adminfee : {},
};

export default function(state = intialState, action) {
  switch (action.type) {
    
    case ADMIN_BOOKING_FEES:
      return {
        ...state,
        adminfee: action.payload,
        error: action.payload.response,
      };
    case SPA_DETAILS_LIST:
      return {
        ...state,
        spaDiscount: action.payload,
        error: action.payload.response,
      };
    
    case FETCH_SPA_DETAILS:
      return {
        ...state,
        spaDetails: action.payload,
        error: action.payload.response,
      };

    case MASSEUSE_PER_SPA:
      return {
        ...state,
        allMasseuse: action.payload,
        error: action.payload.response,
      };

    case FETCH_SERVICES:
      return {
        ...state,
        allServices: action.payload,
        error: action.payload.response,
      };
      
    case SELECTED_SPA:
      return {
        ...state,
        selectedSpaDetails: action.payload,
      };

    case SELECTED_SERVICE:
      return {
        ...state,
        selectedServiceDetails: action.payload,
      };

      case SERVICES:
      return {
        ...state,
        servicesNames: action.payload,
      };

      case FINAL_BOOKING:
      return {
        ...state,
        finalBookingResponse: action.payload,
      };

      case ADMIN_PAYMENT_DETAILS:
      return {
        ...state,
        adminPaymentResponse: action.payload,
        error: action.payload.response,
      };

    default:
      return state;
  }
}
